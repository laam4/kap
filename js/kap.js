//////////////////////////////
// DEFAULT CHANNEL
// the channel to read from if no channel is specified
// you might have to change this instead if you save a local copy of this
//////////////////////////////
var default_channel = "sovietwomble";
//////////////////////////////

// Taken from http://stackoverflow.com/questions/901115/how-can-i-get-query-string-values cuz lazy
function getParameterByName(name) {
	name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	results = regex.exec(location.search);
	return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function calculateColorBackground(color) {
	// Converts HEX to YIQ to judge what color background the color would look best on
	color = String(color).replace(/[^0-9a-f]/gi, '');
	if (color.length < 6) {
		color = color[0] + color[0] + color[1] + color[1] + color[2] + color[2];
	}
	var r = parseInt(color.substr(0, 2), 16);
	var g = parseInt(color.substr(2, 2), 16);
	var b = parseInt(color.substr(4, 2), 16);
	var yiq = ((r * 299) + (g * 587) + (b * 114)) / 1000;
	//return (yiq >= 128) ? "dark" : "light";
	return (yiq >= 128) ? "#212121" : "#DEDEDE";
}

function calculateColorReplacement(color, background) {
	// Modified from http://www.sitepoint.com/javascript-generate-lighter-darker-color/
	var inputColor = color,
		rgb = "#",
		brightness, c, i;

	color = String(color).replace(/[^0-9a-f]/gi, '');
	if (color.length < 6) {
		color = color[0] + color[0] + color[1] + color[1] + color[2] + color[2];
	}

	(background === "light") ? (brightness = "0.2") : (brightness = "-0.5");

	for (i = 0; i < 3; i++) {
		c = parseInt(color.substr(i * 2, 2), 16);
		if(c < 10) c = 10;
		c = Math.round(Math.min(Math.max(0, c + (c * brightness)), 255)).toString(16);
		rgb += ("00" + c).substr(c.length);
	}

	if(inputColor === rgb) {
		if(background === "light") {
			return "#ffffff";
		} else {
			return "#000000";
		}
	} else {
		return rgb;
	}
}

function tagCSS(type, version, url) {
	var $css = $('<style></style>');
	$css.attr('type', 'text/css');
	$css.html('.' + type + '-' + version + ' { background-image: url("' + url.replace('http:', 'https:') + '"); }');
	return $css;
}

function transformBadges(sets) {
	return Object.keys(sets).map(function(b) {
		var badge = sets[b];
		badge.type = b;
		badge.versions = Object.keys(sets[b].versions).map(function(v) {
			var version = sets[b].versions[v];
			version.type = v;
			return version;
		});
		return badge;
	});
}

function getTwitchAPI(url) {
	return $.getJSON(url + (url.indexOf('?') > -1 ? '&' : '?') + "client_id=apbhlybpld3ybc6grv5c118xqpoz01c&callback=?");
}

// Emoticon Code taken from Twitch
Chat = {
	clearChat: function(nick) {
		$('.chat_line[data-nick='+nick+']').remove();
	},
	escape: function(message) {
		return message.replace(/</g,'&lt;').replace(/>/g, '&gt;');
	},
	extraEmoteTemplate: function(emote) {
		return '<img class="emoticon ' + emote.source + '-emo-' + emote.id + '" src="http:' + emote['1x'] + '" srcset="http:' + emote['2x'] + ' 2x" />';
	},
	emoteTemplate: function(id) {
		return '<img class="emoticon ttv-emo-' + id + '" src="http://static-cdn.jtvnw.net/emoticons/v1/' + id + '/1.0" srcset="http://static-cdn.jtvnw.net/emoticons/v1/' + id + '/2.0 2x" />';
	},
	cheerTemplate: function(url, amount) {
		return '<img class="emoticon cheermote" src="' + url + '" /> ' + amount;
	},
	emoticonize: function(message, emotes) {
		if(!emotes) return [message];
		var tokenizedMessage = [];
		var emotesList = Object.keys(emotes);
		var replacements = [];
		emotesList.forEach(function(id) {
			var emote = emotes[id];
			for(var i=emote.length-1; i>=0; i--) {
				replacements.push({ id: id, first: emote[i][0], last: emote[i][1] });
			}
		});

		replacements.sort(function(a, b) {
			return b.first - a.first;
		});
		// Tokenizes each character into an array
		// punycode deals with unicode symbols on surrogate pairs
		// punycode is used in the replacements loop below as well
		message = punycode.ucs2.decode(message);
		replacements.forEach(function(replacement) {
			// Unshift the end of the message (that doesn't contain the emote)
			tokenizedMessage.unshift(punycode.ucs2.encode(message.slice(replacement.last+1)));
			// Unshift the emote HTML (but not as a string to allow us to process links and escape html still)
			tokenizedMessage.unshift([ Chat.emoteTemplate(replacement.id) ]);
			// Splice the unparsed piece of the message
			message = message.slice(0, replacement.first);
		});
		// Unshift the remaining part of the message (that contains no emotes)
		tokenizedMessage.unshift(punycode.ucs2.encode(message));
		return tokenizedMessage;
	},
	extraEmoticonize: function(sender, message, emote) {
		if(emote.restrictions) {
			if(emote.restrictions.channels && emote.restrictions.channels.length && emote.restrictions.channels.indexOf(Chat.vars.channel) === -1) return message;
			if(emote.restrictions.games && emote.restrictions.games.length) return message;
		}
		return message.replace(emote.code, Chat.extraEmoteTemplate(emote));
	},
	getCheer: function(prefix, amount) {
		var amounts = Chat.vars.cheers[prefix];
		return amounts[Object.keys(amounts).sort(function(a, b) {
			return parseInt(b, 10) - parseInt(a, 10);
		}).find(function(a) {
			return amount >= a;
		})];
	},
	findCheerInToken: function(token) {
		var cheerPrefixes = Object.keys(Chat.vars.cheers);
		var tokenLower = token.toLowerCase();
		for (var i = 0; i < cheerPrefixes.length; i++) {
			var prefixLower = cheerPrefixes[i].toLowerCase();
			if (tokenLower.startsWith(prefixLower)) {
				var amount = parseInt(tokenLower.substr(prefixLower.length), 10);
				return Chat.getCheer(cheerPrefixes[i], amount);
			}
		}
		return null;
	},
	findCheerAmount: function(token) {
		var cheerPrefixes = Object.keys(Chat.vars.cheers);
		var tokenLower = token.toLowerCase();
		for (var i = 0; i < cheerPrefixes.length; i++) {
			var prefixLower = cheerPrefixes[i].toLowerCase();
			if (tokenLower.startsWith(prefixLower)) {
				return parseInt(tokenLower.substr(prefixLower.length), 10);
			}
		}
		return 0;
	},
	extraMessageTokenize: function(sender, message) {
		var tokenizedString = message.split(' ');
		for(var i=0; i<tokenizedString.length; i++) {
			var piece = tokenizedString[i];
			var test = piece.replace(/(^[~!@#$%\^&\*\(\)]+|[~!@#$%\^&\*\(\)]+$)/g, '');
			var emote = Chat.vars.extraEmotes[test] || Chat.vars.extraEmotes[piece];
			if (!emote && Chat.vars.proEmotes[sender.name] !== undefined) {
				emote = Chat.vars.proEmotes[sender.name][test] || Chat.vars.proEmotes[sender.name][piece];
			}
			var cheer = Chat.findCheerInToken(piece);
			var cheer_amount = Chat.findCheerAmount(piece);
			if(cheer) {
				piece = Chat.cheerTemplate(cheer, cheer_amount);
			} else if(emote) {
				piece = Chat.extraEmoticonize(sender, piece, emote);
			} else {
				piece = Chat.escape(piece);
			}
			tokenizedString[i] = piece;
		}
		return tokenizedString.join(' ');
	},
	insert: function(nick, tags, message) {
		var nick = nick || "",
		userData = userData || {},
		message = message || "",
		action = action || false;
		tags = tags ? Chat.parseTags(nick, tags) : {};
		if(/^\x01ACTION.*\x01$/.test(message)) {
			action = true;
			message = message.replace(/^\x01ACTION/, '').replace(/\x01$/, '').trim();
		}
		console.log(tags);
		if(nick == "USERNOTICE" && typeof tags.msgType != "undefined") {
			switch(tags.msgType) {
				case 'sub': // $nick subscribed at $tier.
					console.log('sub');
					nick = tags.displayName ? tags.displayName : tags.login;
					nick = `${nick} <span class='usernotice'>New ${tags.subPlan} Sub</span>`;
					break;
				case 'resub': // $nick subscribed with/at $tier. They've subscribed for $x months, currently on a $x month streak!
					console.log('reeesub');
					nick = tags.displayName ? tags.displayName : tags.login;
					if(tags.shareStreak == '1') {
						nick = `${nick} <span class='usernotice'>${tags.subPlan} Resub ${tags.Months}x, Streak ${tags.streakMonths}x</span>`;
					} else {
						nick = `${nick} <span class='usernotice'>${tags.subPlan} Resub ${tags.Months}x</span>`;
					}
					break;
				case 'subgift': // $nick gifted a Tier 1 sub to $nick!"
					console.log('giftsub');
					nick = tags.displayName ? tags.displayName : tags.login;
					nick = `${nick} <span class='usernotice'>${tags.subPlan} Giftsub to</span> ${tags.recipient}`;
					break;
				case 'anonsubgift': // An anonymous user gifted a Tier 1 sub to $nick!"
					console.log('anongiftsub');
					//nick = tags.displayName ? tags.displayName : tags.login;
					nick = `AnAnonymousGifter <span class='usernotice'>${tags.subPlan} Giftsub to ${tags.recipient}</span>`;
					break;
				case 'submysterygift': // $nick is gifting 5 Tier 1 Subs to $streamer community! They've gifted a total of $x in the channel!
					console.log('submysterygift');
					nick = `${nick} <span class='usernotice'>${tags.subPlan} Giftsub</span>`;
					break;
				case 'giftpaidupgrade': // $nick is continuing the Gift Sub they got from $nick!
					console.log('giftpaidupgrade')
					break;
				case 'rewardgift':
					console.log('rewardgift');
					break;
				case 'anongiftpaidupgrade':
					console.log('anongiftpaidupgrade');
					break;
				case 'raid':
					console.log('raid');
					nick = `${tags.raid} <span class='usernotice'>Raid with ${tags.raiders} viewers</span>`;
					break;
				case 'unraid':
					console.log('unraid');
					break;
				case 'ritual':
					console.log('ritual');
					break;
				case 'bitsbadgetier':
					console.log('bitsbadgetier');
					break;
				case 'extendsub': // $nick extended their $tier subscription through July!
					console.log('extendsub');
					break;
				case 'communitypayforward': // $nick is paying forward the Gift they got from CashApp to the community!
					console.log('communitypayforward');
					break;
			}
		} else if(typeof tags.displayName != "undefined") {
			if (tags.displayName.toLowerCase() != tags.name) {
				console.log(nick +"+"+ tags.displayName +"+"+ tags.name);
				nick = `${tags.displayName} <span class='login'>(${tags.name})</span>`;
			} else {
				nick = tags.displayName
			}
		}
		var $newLine = $('<div></div>');
		var $newMsg = $('<span></span>');
		$newMsg.addClass('chat_line');
		$newMsg.attr('data-nick', nick);
		$newMsg.attr('data-user-type', tags.userType);
		$newMsg.attr('data-timestamp', Date.now());
		//var $time = $('<span></span>');
		//$time.addClass('time_stamp')
		//$time.text(new Date().toLocaleTimeString().replace(/^(\d{0,2}):(\d{0,2}):(.*)$/i, '$1:$2'));
		//$newLine.append($time);
		var $nickBackground = $('<span></span>');
		$nickBackground.addClass('nick-bg');
		/*if (getParameterByName('nickcolor').toLowerCase()) {
			$formattedUser.css('background-color', getParameterByName('nickcolor').toLowerCase());
		} else {*/
		$nickBackground.css('background-color', tags.color);
		//}
		$nickBackground.css('color', calculateColorBackground(tags.color));
		var $nick = $('<span></span>');
		$nick.addClass('nick');
		//$formattedUser.html(tags.displayName ? tags.displayName : nick);
		$nick.html(nick);
		
		if(tags.badges) {
			tags.badges.forEach(function(badge) {
				var $tagbg = $('<span></span>');
				var $tag = $('<span></span>');
				$tagbg.addClass(badge.type + '-' + badge.version + '-bg');
				$tagbg.addClass('tag-bg');
				$tag.addClass(badge.type + '-' + badge.version);
				$tag.addClass('tag')
				$tag.html('&nbsp;');
				//$newLine.append($tag);
				$tagbg.append($tag);
				$nickBackground.append($tagbg);
			});
		}
		$nickBackground.append($nick);

		/*if((!Chat.vars.themeNameCC && Chat.vars.style !== 'clear') || (Chat.vars.themeNameCC && Chat.vars.themeNameCC.enabled)) {
			var bg = (Chat.vars.style !== 'clear') ? Chat.vars.style : Chat.vars.themeNameCC.kind;
			if(/^#[0-9a-f]+$/i.test(tags.color)) {
				while(calculateColorBackground(tags.color) !== bg) {
					tags.color = calculateColorReplacement(tags.color, calculateColorBackground(tags.color));
				}
			}
		}*/
		$newMsg.append($nickBackground);
		//$newLine.append('&nbsp;')
		//action ? $newLine.append('&nbsp;') : $newLine.append('<span class="colon"></span>&nbsp;');
		var $msgBackground = $('<span></span>');
		$msgBackground.addClass('msg-bg');
		if(action) {
			$msgBackground.css('color', calculateColorBackground(tags.color));
			$msgBackground.css('background-color', tags.color);
		}
		var emotes = {};
		if(tags.emotes) {
			tags.emotes = tags.emotes.split('/');
			tags.emotes.forEach(function(emote) {
				emote = emote.split(':');
				if(!emotes[emote[0]]) emotes[emote[0]] = [];
				var replacements = emote[1].split(',');
				replacements.forEach(function(replacement) {
					replacement = replacement.split('-');
					emotes[emote[0]].push([ parseInt(replacement[0]) , parseInt(replacement[1]) ]);
				});
			});
		}
		var tokenizedMessage = Chat.emoticonize(message, emotes);
		for(var i=0; i<tokenizedMessage.length; i++) {
			if(typeof tokenizedMessage[i] === 'string') {
				tokenizedMessage[i] = Chat.extraMessageTokenize(tags, tokenizedMessage[i]);
			} else {
				tokenizedMessage[i] = tokenizedMessage[i][0];
			}
		}
		message = tokenizedMessage.join(' ');
		var $msg = $('<span></span>');
		$msg.addClass('msg');
		$msg.html(message);
		$msgBackground.html($msg);
		$newMsg.append($msgBackground);
		$newLine.append($newMsg);
		Chat.vars.queue.push($newLine.html());
	},
	load: function(channel) {
		Chat.vars.channel = channel;
		if(Chat.vars.theme) {
			Chat.loadTheme(Chat.vars.theme);
		} else {
			$('#chat_box').attr('class', Chat.vars.style);
		}
		/*if(Chat.vars.preventClipping) {
			Chat.vars.max_height = $(window).height();
		} else {
			$('#chat_box').css('height', 'calc(100% - 10px)');
		}*/
		Chat.loadCheers();
		Chat.loadEmotes(function() {
			Chat.loadGlobalBadges(function() {
				var socket = new ReconnectingWebSocket('wss://irc-ws.chat.twitch.tv', 'irc', { reconnectInterval: 3000 });
				socket.onopen = function(data) {
					Chat.insert("&#x1F7E2", null, "Connect");
					socket.send('PASS blah\r\n');
					socket.send('NICK justinfan12345\r\n');
					socket.send('CAP REQ :twitch.tv/commands twitch.tv/tags\r\n');
					socket.send('JOIN #' + Chat.vars.channel + '\r\n');
				};
				socket.onclose = function() {
					Chat.insert("&#x1F534", null, "Disconnect");
				};
				socket.onmessage = function(data) {
					var message = window.parseIRC(data.data.trim());
					//console.log(message);
					if(!message.command) return;
					switch(message.command) {
						case "PING":
							socket.send('PONG ' + message.params[0]);
							return;
						case "JOIN":
							//Chat.insert(null, null, "Joined channel: "+Chat.vars.channel+".");
							return;
						case "CLEARCHAT":
							if(message.params[1]) Chat.clearChat(message.params[1]);
							console.log(message);
							return;
						case "PRIVMSG":
							if(message.params[0] !== '#' + channel || !message.params[1]) return;
							var nick = message.prefix.split('@')[0].split('!')[0];
							if(getParameterByName('bot_activity').toLowerCase() !== 'true') {
								if(message.params[1].charAt(0) === '!') return;
								if(/bot$/.test(nick)) return;
							}
							if (Chat.vars.spammers.indexOf(nick) > -1) return;
							Chat.insert(nick, message.tags, message.params[1]);
							return;
						case "USERNOTICE":
							console.log(message.tags);
							Chat.insert("USERNOTICE", message.tags, message.params[1]);
							return;
						default:
							console.log(message);
					}
				};
				Chat.vars.socket = socket;
				var bttvSocket = new ReconnectingWebSocket('wss://sockets.betterttv.net/ws', null, { reconnectInterval: 3000 });
				bttvSocket.emit = function(evt, data) {
				this.send(JSON.stringify({
					name: evt,
					data: data
				}));
				}
				bttvSocket.events = {};
				bttvSocket.events.new_spammer = function(data) {
					console.log('Added ' + data.name + 'as a spammer');
					Chat.vars.spammers.push(data.name);
					Chat.clearChat(data.name);
				};
				bttvSocket.events.lookup_user = function(subscription) {
					if (!subscription.pro && !subscription.subscribed) return;
					if (subscription.pro && subscription.emotes) {
						console.log('Added BTTV pro emotes for ' + subscription.name);
						Chat.vars.proEmotes[subscription.name] = {};
						subscription.emotes.forEach(function(emote) {
							Chat.vars.proEmotes[subscription.name][emote.code] = {
								restrictions: emote.restrictions,
								code: emote.code,
								source: 'bttv',
								id: emote.id,
								'1x': 'cdn.betterttv.net/emote/' + emote.id + '/1x',
								'2x': 'cdn.betterttv.net/emote/' + emote.id + '/2x'
							};
						});
					}
				};
				bttvSocket.onopen = function() {
					var channel = Chat.vars.channel;
					if (!channel.length) return;
					if (this._joinedChannel) {
						this.emit('part_channel', { name: this._joinedChannel });
					}
					this.emit('join_channel', { name: channel });
					this._joinedChannel = channel;
				}
				bttvSocket.onmessage = function(message) {
					var evt = JSON.parse(message.data);
					if (!evt || !(evt.name in this.events)) return;
					this.events[evt.name](evt.data);
				}
				Chat.vars.bttvSocket = bttvSocket;
			});
		});
	},
	loadEmotes: function(callback) {
		if (!getParameterByName('nobttv').toLowerCase()) {
			getTwitchAPI("https://api.twitch.tv/v5/users?login=" + Chat.vars.channel).done(function(e) {
				['cached/emotes/global', 'cached/users/twitch/' + e.users[0]._id].forEach(function(endpoint) {
					$.getJSON('https://api.betterttv.net/3/' + endpoint).done(function(data) {
						//console.log(data);
						if (data.channelEmotes != null) {
							console.log(data.sharedEmotes);
							console.log(data.channelEmotes);
							data = data.sharedEmotes.concat(data.channelEmotes)
							//console.log(asdf);
						}
						data.forEach(function(emote) {
							Chat.vars.extraEmotes[emote.code] = {
								restrictions: emote.restrictions,
								code: emote.code,
								source: 'bttv',
								id: emote.id,
								'1x': 'cdn.betterttv.net/emote/' + emote.id + '/1x',
								'2x': 'cdn.betterttv.net/emote/' + emote.id + '/2x'
							};
						});
					});
				});
			});	
		}
		if (!getParameterByName('noffz').toLowerCase()) {
			['set/global', 'room/' + encodeURIComponent(Chat.vars.channel)].forEach(function(endpoint) {
				$.getJSON('https://api.frankerfacez.com/v1/' + endpoint).done(function(data) {
					if(typeof data.sets !== 'object') return;
					Object.keys(data.sets).forEach(function(set) {
						set = data.sets[set];
						if(!set.emoticons || !Array.isArray(set.emoticons)) return;
						set.emoticons.forEach(function(emoticon) {
							if(!emoticon.name || !emoticon.id) return;
							if(typeof emoticon.name !== 'string' || typeof emoticon.id !== 'number') return;
							if(Chat.vars.extraEmotes[emoticon.name]) return;
							if(!emoticon.urls || typeof emoticon.urls !== 'object') return;
							if(typeof emoticon.urls[1] !== 'string') return;
							if(emoticon.urls[2] && typeof emoticon.urls[2] !== 'string') return;
							Chat.vars.extraEmotes[emoticon.name] = {
								source: 'ffz',
								code: emoticon.name,
								id: emoticon.id,
								'1x': emoticon.urls[1],
								'2x': emoticon.urls[2] || emoticon.urls[1].replace(/1$/, '2')
							};
						});
					});
				});
			});
		}
		callback(true);
	},
	loadTheme: function(id) {
		$.getJSON("themes/" + encodeURIComponent(id) + ".json").done(function(e) {
			if(!e.key) return;
			var $css = $('<style></style>');
			$css.attr('type', 'text/css');
			$css.html(e.css);
			$("head").append($css);
			Chat.vars.themeNameCC = e.nameCC;
		});
	},
	loadSubscriberBadge: function(callback) {
		getTwitchAPI("https://api.twitch.tv/v5/users?login=" + Chat.vars.channel).done(function(e) {
			getTwitchAPI("https://badges.twitch.tv/v1/badges/channels/" + e.users[0]._id + "/display").done(function(e) {
				transformBadges(e.badge_sets).forEach(function(badge) {
					badge.versions.forEach(function(version) {
						$("head").append(tagCSS(badge.type, version.type, version.image_url_4x));
					});
				});
				callback(true);
			});
		});	
	},
	loadGlobalBadges: function(callback) {
		getTwitchAPI("https://badges.twitch.tv/v1/badges/global/display").done(function(e) {
			transformBadges(e.badge_sets).forEach(function(badge) {
				badge.versions.forEach(function(version) {
					$("head").append(tagCSS(badge.type, version.type, version.image_url_4x));
				});
			});
			Chat.loadSubscriberBadge(callback);
		});
	},
	loadCheers: function() {
		getTwitchAPI("https://api.twitch.tv/v5/bits/actions").done(function(e) {
			try {
				e.actions.forEach(function(action) {
					var cheer = Chat.vars.cheers[action.prefix] = {};
					action.tiers.forEach(function(tier) {
						cheer[tier.min_bits] = tier.images.light.animated['4'];
					});
				});
			} catch(e) {}
		});
	},
	parseTags: function(nick, tags) {
		var defaultColors = ["#FF0000", "#0000FF", "#008000", "#B22222", "#FF7F50", "#9ACD32", "#FF4500", "#2E8B57", "#DAA520", "#D2691E", "#5F9EA0", "#1E90FF", "#FF69B4", "#8A2BE2", "#00FF7F"];
		var res = {
			name: nick,
			displayName: nick,
			color: defaultColors[nick.charCodeAt(0) % 15],
			sysMsg: null,
			emotes: null,
			badges: [],
			msgType: null,
			//cumulativeMonths: null,
			Months: null,
			shareStreak: null,
			subPlan: null,
			streakMonths: null,
			login: null,
			recipient: null,
			raid: null,
			raiders: null
		};
		if(tags['display-name'] && typeof tags['display-name'] === 'string') {
			res.displayName = tags['display-name'];
		}
		if(tags.color && typeof tags.color === 'string') {
			res.color = tags.color;
		}
		if(tags.emotes && typeof tags.emotes === 'string') {
			res.emotes = tags.emotes;
		}
		if(tags.badges && typeof tags.badges === 'string') {
			res.badges = tags.badges.split(',').map(function(badge) {
				badge = badge.split('/');
				return {
					type: badge[0],
					version: badge[1]
				};
			});
		}
		if(tags['system-msg'] && typeof tags['system-msg'] === 'string') {
			res.sysMsg = tags['system-msg'];
			//res.sysMsg.replace(/\s/g, ' ');
			console.log(res.sysMsg);
		}
		if(tags['msg-id'] && typeof tags['msg-id'] === 'string') {
			res.msgType = tags['msg-id'];
		}
		if(tags['msg-param-cumulative-months'] && typeof tags['msg-param-cumulative-months'] === 'string') {
			res.Months = tags['msg-param-cumulative-months'];
		}
		/*if(tags['msg-param-months'] && typeof tags['msg-param-months'] === 'string') {
			res.Months = tags['msg-param-months'];
		}*/
		if(tags['msg-param-should-share-streak'] && typeof tags['msg-param-should-share-streak'] === 'string') {
			res.shareStreak = tags['msg-param-should-share-streak'];
		}
		if(tags['msg-param-sub-plan'] && typeof tags['msg-param-sub-plan'] === 'string') {
			switch(tags['msg-param-sub-plan']) {
				case '1000':
					res.subPlan = 'Tier 1';
					break;
				case '2000':
					res.subPlan = 'Tier 2';
					break;
				case '3000':
					res.subPlan = 'Tier 3';
					break;
				case 'Prime':
					res.subPlan = 'Prime';
					break;
			}
			//res.subPlan = tags['msg-param-sub-plan'];
		}
		if(tags['msg-param-streak-months'] && typeof tags['msg-param-streak-months'] === 'string') {
			res.streakMonths = tags['msg-param-streak-months'];
		}
		if(tags.login && typeof tags.login === 'string') {
			res.login = tags.login;
		}
		if(tags['msg-param-recipient-display-name'] && typeof tags['msg-param-recipient-display-name'] === 'string') {
			res.recipient = tags['msg-param-recipient-display-name'];
		}
		if(tags['msg-param-displayName'] && typeof tags['msg-param-displayName'] === 'string') {
			res.raid = tags['msg-param-displayName'];
		}
		if(tags['msg-param-viewerCount'] && typeof tags['msg-param-viewerCount'] === 'string') {
			res.raiders = tags['msg-param-viewerCount'];
		}
		//console.log(res);
		return res;
	},
	vars: {
		queue: [],
		maxDisplayTime: getParameterByName('fade') === 'true' ? 30 : parseInt(getParameterByName('fade')),
		//maxDisplayTime: 99999,
		queueTimer: setInterval(function() {
			if(Chat.vars.queue.length > 0) {
				var newLines = Chat.vars.queue.join('');
				Chat.vars.queue = [];
				var old_width = $('#chat_box').width();
				$('#chat_box').append(newLines + " ");
				var offset = old_width - $('#chat_box').width();
				$('#chat_box').css('transition','right 0s');
				$('#chat_box').css('right', offset + 'px');
				setTimeout(function() {
					$('#chat_box').css('transition','right 2s');
					$('#chat_box').css('right','5px');
				}, 10);
				/*if(Chat.vars.preventClipping) {
					var totalHeight = Chat.vars.max_height;
					var currentHeight = $('#chat_box').outerHeight(true) + 5;
					var count = 0;
					var $chatLine, lineHeight;
					while(currentHeight > totalHeight) {
						$chatLine = $('.chat_line').eq(count);
						lineHeight = $chatLine.height();
						$chatLine.animate({	"margin-top": -lineHeight }, 100, function() { $(this).remove(); });
						currentHeight -= lineHeight;
						count++;
					}
					return;
				}
				$('#chat_box')[0].scrollTop = $('#chat_box')[0].scrollHeight;*/
				var linesToDelete = $('#chat_box .chat_line').length - Chat.vars.max_messages;
				if(linesToDelete > 0) {
					for(var i=0; i<linesToDelete; i++) {
						$('#chat_box .chat_line').eq(0).remove();
					}
				}
			} else if(getParameterByName('fade')) {
				var messagePosted = $('#chat_box .chat_line').eq(0).data('timestamp');
				if((Date.now()-messagePosted)/1000 >= Chat.vars.maxDisplayTime) {
					$('#chat_box .chat_line').eq(0).addClass('on_out').fadeOut(function() {
						$(this).remove();
					});
				}
			}
		}, 250),
		style: getParameterByName('style').toLowerCase() || 'clear',
		theme: getParameterByName('theme').toLowerCase(),
		preventClipping: getParameterByName('prevent_clipping') === 'true' ? true : false,
		themeNameCC: null,
		socket: null,
		bttvSocket: null,
		channel: null,
		max_messages: 50,
		max_height: 720,
		extraEmotes: {},
		proEmotes: {},
		spammers: [],
		cheers: {}
	}
};

$(document).ready(function(){
		Chat.load(getParameterByName('channel').toLowerCase() || default_channel);
});