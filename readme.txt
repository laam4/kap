KapChat by NightDev

Very slight modification by Dotsarecool

Go to https://nightdev.com/kapchat/ for info on KapChat.

---------------------------
ONE IMPORTANT THING TO NOTE
---------------------------

OBS Studio Browser source has a "local file" checkbox for using a local file as a source.
Don't check this box since you can't add paramaters to the end of the URL.
Instead, just attached the file location to the "file://" prefix.

So, instead of

C:/Users/You/Documents/SideKapChat/KapChat.html?channel=yourchannel

it should be

file://C:/Users/You/Documents/SideKapChat/KapChat.html?channel=yourchannel




accepted parameters:
	channel
		*channel* - your twitch channel name to poll chat from
	theme
		none - no theme: useful for css stylizing yourself in obs
		bttv_blackchat
		s0n0s_1440
		bttv_dark
		bttv_light
		dark
		s0n0s_1080
		light
	style
		clear
	fade
		false - default. never erase messages
		true - erase messages after 30 seconds
		*a number* - number of seconds before a chat message disappears
	noffz
		true - default. show ffz emotes
		false - don't parse ffz emotes
	nobttv
		true - default. show bttv emotes
		false - don't parse bttv emotes
	bot_activity
		false - default. disable bot messages in chat
		true - enable bot messages in chat
	nickcolor
		*color name or hexcode* - setting this to a color will change all names to the same color

parameter list starts with ? and concatenates with &
add them to the end of the URL

example:

file://C:/Users/Dots/Documents/SideKapChat/KapChat.html?channel=dotsarecool&bot_activity=true&nobttv=true